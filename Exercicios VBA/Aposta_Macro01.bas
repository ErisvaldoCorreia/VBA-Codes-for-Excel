Attribute VB_Name = "M�dulo2"
' DESENVOLVEDOR: ERISVALDO CORREIA
' Codigo para gerar um sistema de apostas VBA
' Usa refer�ncias de c�lulas do Excel para exibir valores!

Sub Aposta()
    
    'Declara��o das variaveis
    Dim NAposta, NSorteio, TotTentativas As Integer
    Dim cansou As Boolean
    Dim quest
    
    'Inicializa��o da Macro
    cansou = True
    Cells(5, 5).Value = ""
    Cells(8, 6).Value = ""
    TotTentativas = 0
    
    'la�o de repeti��o baseado em condi��o True (verdadeiro)
    While cansou
        NAposta = InputBox("Digite o valor da sua aposta entre (1 e 50)")
        NSorteio = Sorteio()
        TotTentativas = TotTentativas + 1
        
        'Verifica��o condicional para exibi��o de resultado!
        If NAposta = NSorteio Then
            'Resposta positiva
            MsgBox "Parab�ns! Acertou, o valor sorteado foi " & NSorteio
            Cells(5, 5).Value = "Venceu o Jogo!"
            Cells(8, 6).Value = TotTentativas 'Numero de tentativas usadas
            cansou = False
        Else
            'Resposta negativa
            MsgBox "Continue tentando, o valor sorteado foi " & NSorteio
            quest = MsgBox("Deseja Continuar?", vbYesNo, "Continua?")
            
            If quest <> 7 Then
                'recebe True se o valor for 6 (bot�o YES)
                cansou = True
            Else
                'recebe False se o valor for 7 (boat�o NO)
                cansou = False
                Cells(5, 5).Value = "Desistiu do Jogo"
                Cells(8, 6).Value = TotTentativas 'Numero de tentativas usadas
            End If
        End If
    Wend

End Sub

' Fun��o chamada na rotina da 'aposta'
Function Sorteio() As Integer
    
    'Realiza um sorteio randomico entre 1 e 50
    Sorteio = Int(Rnd() * (50 - 1) + 1)

End Function


